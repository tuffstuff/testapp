package com.example.nursultan.testapp.ui.photo;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.example.nursultan.testapp.R;
import com.example.nursultan.testapp.di.component.ActivityComponent;
import com.example.nursultan.testapp.dummy.DummyContent;
import com.example.nursultan.testapp.ui.base.BaseFragment;
import com.example.nursultan.testapp.ui.detail.DetailFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoFragment extends BaseFragment implements PhotoAdapter.Callback, PhotoMvpView {

    private static final String TAG = "PhotoFragment";

    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.go_btn) Button goBtn;
    @BindView(R.id.autoCompleteTextView) AutoCompleteTextView _et;

    @Inject
    PhotoMvpPresenter<PhotoMvpView> presenter;
    @Inject
    PhotoAdapter adapter;
    @Inject
    GridLayoutManager manager;
    @Inject
    ArrayAdapter suggestionsAdapter;

    public PhotoFragment() {
    }


    @SuppressWarnings("unused")
    public static PhotoFragment newInstance(int columnCount) {
        PhotoFragment fragment = new PhotoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_list, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
            adapter.setCallback(this);
        }
        return view;
    }


    protected void setUp(View view) {
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        _et.setThreshold(1);
        _et.setAdapter(suggestionsAdapter);

        presenter.onViewPrepared();

    }

    @Override
    public void updateData(List<DummyContent.DummyItem> list) {
        adapter.swapItems(list);
    }

    @Override
    public void updateSuggestions(List<String> list) {
        if (list != null && list.size() > 0) {
            suggestionsAdapter.clear();
            suggestionsAdapter.addAll(list);
        }
    }

    @OnClick(R.id.go_btn)
    public void click () {
        presenter.insertSearchQuery(_et.getText().toString());
        presenter.requestSearchUpdate(_et.getText().toString());
    }

    @Override
    public void onDestroyView() {
        presenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void showFull(String path) {
        getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, DetailFragment.newInstance(path))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onPhotoClick(DummyContent.DummyItem item) {
        presenter.goDetail(item.path);
    }
}

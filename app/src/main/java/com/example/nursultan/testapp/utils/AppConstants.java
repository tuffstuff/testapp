package com.example.nursultan.testapp.utils;

public final class AppConstants {

    public static final String DB_NAME = "mindorks_mvp.db";
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    public static final long NULL_INDEX = -1L;
    public static final int SPAN_COUNT = 3;
    public static final String BASE_URL = "http://167.99.199.39/";

    private AppConstants() {
        // not publicly instantiable
    }
}

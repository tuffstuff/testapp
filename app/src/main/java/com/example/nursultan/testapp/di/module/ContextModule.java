package com.example.nursultan.testapp.di.module;

import android.app.Application;
import android.content.Context;

import com.example.nursultan.testapp.TestApp;
import com.example.nursultan.testapp.data.AppDataManager;
import com.example.nursultan.testapp.data.DataManager;
import com.example.nursultan.testapp.data.db.AppDbHelper;
import com.example.nursultan.testapp.data.db.DbHelper;
import com.example.nursultan.testapp.di.ApplicationContext;
import com.example.nursultan.testapp.di.DatabaseInfo;
import com.example.nursultan.testapp.di.TestApplicationScope;
import com.example.nursultan.testapp.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    private final Context context;

    public ContextModule (Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides
    @TestApplicationScope
    @ApplicationContext
    public Context context () {
        return this.context;
    }


    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @TestApplicationScope
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @TestApplicationScope
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }
}

package com.example.nursultan.testapp.ui.photo;

import com.example.nursultan.testapp.data.DataManager;
import com.example.nursultan.testapp.data.db.model.Query;
import com.example.nursultan.testapp.dummy.DummyContent;
import com.example.nursultan.testapp.network.ApiService;
import com.example.nursultan.testapp.ui.base.BasePresenter;
import com.example.nursultan.testapp.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class PhotoPresenter<V extends PhotoMvpView> extends BasePresenter<V>
        implements PhotoMvpPresenter<V> {

    @Inject
    public PhotoPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable,
                          ApiService apiService) {
        super(dataManager, schedulerProvider, compositeDisposable, apiService);
    }


    @Override
    public void onViewPrepared() {
        getQueries();
        getMvpView().updateData(DummyContent.ITEMS);
    }

    @Override
    public void requestSearchUpdate(final String queryText) {
        if (queryText.trim().equals("")) {
            return;
        }
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getPhotos()
                .subscribeOn(getSchedulerProvider().computation())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<DummyContent.DummyItem>>() {
                    @Override
                    public void accept(@NonNull List<DummyContent.DummyItem> list)
                            throws Exception {
                        getMvpView().updateData(filterList(queryText, list));
                        // update adapter data
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                        // handle the error here
                    }
                }));
    }

    @Override
    public void insertSearchQuery(String queryText) {
        if (queryText.trim().equals("")) {
            return;
        }
        Query query = new Query();
        query.setQueryText(queryText);
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .insertQuery(query)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long insertResponse)
                            throws Exception {
                        getQueries();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        // handle the error here
                    }
                }));
    }

    @Override
    public void getQueries() {
        getCompositeDisposable().add(getDataManager()
                .getAllQueries()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer< List<Query> >() {
                    @Override
                    public void accept(@NonNull List<Query> queries)
                            throws Exception {
                        List<String> list = removeDuplicates(queries);
                        if (list.size() > 0) {
                            getMvpView().updateSuggestions(list);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        // handle the error here
                    }
                }));
    }

    @Override
    public void goDetail(String path) {
        getMvpView().showFull(path);
    }

    private List<String> removeDuplicates(List<Query> queries) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < queries.size(); i++) {
            if (queries.get(i).getQueryText() != null) {
                list.add(queries.get(i).getQueryText());
            }
        }
        Set<String> hs = new HashSet<>();
        hs.addAll(list);
        list.clear();
        list.addAll(hs);
        return list;
    }

    private List<DummyContent.DummyItem> filterList(String queryText, List<DummyContent.DummyItem> list) {
        List<DummyContent.DummyItem> filteredList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            DummyContent.DummyItem cur = list.get(i);
            if (cur.title.toLowerCase().contains(queryText)) {
                filteredList.add(cur);
            }
        }
        return filteredList;
    }

}

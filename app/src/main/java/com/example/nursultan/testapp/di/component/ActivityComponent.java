package com.example.nursultan.testapp.di.component;

import com.example.nursultan.testapp.di.PerActivity;
import com.example.nursultan.testapp.di.module.ActivityModule;
import com.example.nursultan.testapp.screens.HomeActivity;
import com.example.nursultan.testapp.ui.detail.DetailFragment;
import com.example.nursultan.testapp.ui.photo.PhotoFragment;

import dagger.Component;


@PerActivity
@Component(dependencies = TestAppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject (PhotoFragment fragment);
    void inject (DetailFragment fragment);
    void inject (HomeActivity activity);
}

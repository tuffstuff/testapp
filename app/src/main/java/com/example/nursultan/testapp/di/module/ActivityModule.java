package com.example.nursultan.testapp.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.widget.ArrayAdapter;

import com.example.nursultan.testapp.di.ActivityContext;
import com.example.nursultan.testapp.ui.detail.DetailMvpPresenter;
import com.example.nursultan.testapp.ui.detail.DetailMvpView;
import com.example.nursultan.testapp.ui.detail.DetailPresenter;
import com.example.nursultan.testapp.ui.photo.PhotoMvpPresenter;
import com.example.nursultan.testapp.ui.photo.PhotoMvpView;
import com.example.nursultan.testapp.ui.photo.PhotoPresenter;
import com.example.nursultan.testapp.utils.AppConstants;
import com.example.nursultan.testapp.utils.rx.AppSchedulerProvider;
import com.example.nursultan.testapp.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {
    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    public Context context () {
        return activity;
    }

    @Provides
    AppCompatActivity activity() {
        return activity;
    }


    @Provides
    GridLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new GridLayoutManager(activity, AppConstants.SPAN_COUNT); // numerical value in app.const
    }

    @Provides
    ArrayAdapter provideArrayAdapter(AppCompatActivity activity) {
        return new ArrayAdapter(activity, android.R.layout.simple_list_item_1);
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    PhotoMvpPresenter<PhotoMvpView> providePhotoMvpPresenter(
            PhotoPresenter<PhotoMvpView> presenter) {
        return presenter;
    }

    @Provides
    DetailMvpPresenter<DetailMvpView> provideDetailMvpPresenter(
            DetailPresenter<DetailMvpView> presenter) {
        return presenter;
    }

}

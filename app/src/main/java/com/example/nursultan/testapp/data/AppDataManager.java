package com.example.nursultan.testapp.data;

import android.content.Context;

import com.example.nursultan.testapp.data.db.DbHelper;
import com.example.nursultan.testapp.data.db.model.Query;
import com.example.nursultan.testapp.di.ApplicationContext;
import com.example.nursultan.testapp.di.TestApplicationScope;
import com.example.nursultan.testapp.dummy.DummyContent;
import com.example.nursultan.testapp.network.ApiService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

@TestApplicationScope
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final ApiService api;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper, ApiService api) {
        mContext = context;
        mDbHelper = dbHelper;
        this.api = api;
    }

    @Override
    public void updateApiHeader() {
        // update header parameters in network client
    }


    @Override
    public Observable<Long> insertQuery(Query query) {
        return mDbHelper.insertQuery(query);
    }

    @Override
    public Observable<List<Query>> getAllQueries() {
        return mDbHelper.getAllQueries();
    }

    @Override
    public Observable<List<DummyContent.DummyItem>> getPhotos() {
        return mDbHelper.getPhotos();
    }

}

package com.example.nursultan.testapp.di.component;

import android.content.Context;

import com.example.nursultan.testapp.TestApp;
import com.example.nursultan.testapp.data.DataManager;
import com.example.nursultan.testapp.di.ApplicationContext;
import com.example.nursultan.testapp.di.TestApplicationScope;
import com.example.nursultan.testapp.di.module.ApiServiceModule;
import com.example.nursultan.testapp.di.module.ContextModule;
import com.example.nursultan.testapp.di.module.PicassoModule;
import com.example.nursultan.testapp.network.ApiService;
import com.squareup.picasso.Picasso;

import dagger.Component;

@TestApplicationScope
@Component(modules = {ApiServiceModule.class, PicassoModule.class, ContextModule.class})
public interface TestAppComponent {
    void inject(TestApp app);

    @ApplicationContext
    Context context();

    DataManager dataManager();
    ApiService api();
    Picasso picasso();
}

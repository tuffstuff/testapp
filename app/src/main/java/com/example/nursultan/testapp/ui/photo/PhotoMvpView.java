package com.example.nursultan.testapp.ui.photo;

import com.example.nursultan.testapp.dummy.DummyContent;
import com.example.nursultan.testapp.ui.base.MvpView;

import java.util.List;

public interface PhotoMvpView extends MvpView {
    void updateData(List<DummyContent.DummyItem> photoList);
    void updateSuggestions(List<String> list);
    void showFull(String path);
}
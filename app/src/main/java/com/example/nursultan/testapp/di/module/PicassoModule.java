package com.example.nursultan.testapp.di.module;

import android.content.Context;

import com.example.nursultan.testapp.di.ApplicationContext;
import com.example.nursultan.testapp.di.TestApplicationScope;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ContextModule.class, NetworkModule.class})
public class PicassoModule {

    @Provides
    @TestApplicationScope
    public Picasso picasso (@ApplicationContext Context context) {
        return new Picasso.Builder(context)
                .build();
    }

}

package com.example.nursultan.testapp.ui.detail;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nursultan.testapp.R;
import com.example.nursultan.testapp.di.component.ActivityComponent;
import com.example.nursultan.testapp.ui.base.BaseFragment;
import com.example.nursultan.testapp.ui.photo.PhotoMvpView;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailFragment extends BaseFragment implements DetailMvpView {

    private static final String PATH_PARAM = "path_param";
    private String mPathParam;

    @BindView(R.id.iv)
    ImageView iv;

    @Inject
    Picasso picasso;
    @Inject
    DetailPresenter<DetailMvpView> presenter;

    public DetailFragment() {
    }


    public static DetailFragment newInstance(String path) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(PATH_PARAM, path);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathParam = getArguments().getString(PATH_PARAM);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_detail, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void loadImage() {
        picasso.load(mPathParam).into(iv);
    }

    @Override
    protected void setUp(View view) {
        presenter.onViewPrepared();
    }

}

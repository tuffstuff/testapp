package com.example.nursultan.testapp.data.db;

import com.example.nursultan.testapp.data.db.model.DaoMaster;
import com.example.nursultan.testapp.data.db.model.DaoSession;
import com.example.nursultan.testapp.data.db.model.Query;
import com.example.nursultan.testapp.di.TestApplicationScope;
import com.example.nursultan.testapp.dummy.DummyContent;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@TestApplicationScope
public class AppDbHelper implements DbHelper {

    private final DaoSession mDaoSession;

    @Inject
    public AppDbHelper(DbOpenHelper dbOpenHelper) {
        mDaoSession = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
    }

    @Override
    public Observable<Long> insertQuery(final Query query) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return mDaoSession.getQueryDao().insert(query);
            }
        });
    }

    @Override
    public Observable<List<Query>> getAllQueries() {
        return Observable.fromCallable(new Callable<List<Query>>() {
            @Override
            public List<Query> call() throws Exception {
                return mDaoSession.getQueryDao().loadAll();
            }
        });
    }

    @Override
    public Observable<List<DummyContent.DummyItem>> getPhotos() {
        return Observable.fromCallable(new Callable<List<DummyContent.DummyItem>>() {
            @Override
            public List<DummyContent.DummyItem> call() throws Exception {
                return DummyContent.ITEMS;
            }
        });
    }
}


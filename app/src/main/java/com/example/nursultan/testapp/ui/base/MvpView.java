package com.example.nursultan.testapp.ui.base;

public interface MvpView {
    void showLoading();

    void hideLoading();

    void hideKeyboard();
}

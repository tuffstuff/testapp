package com.example.nursultan.testapp.ui.detail;

import com.example.nursultan.testapp.data.DataManager;
import com.example.nursultan.testapp.network.ApiService;
import com.example.nursultan.testapp.ui.base.BasePresenter;
import com.example.nursultan.testapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class DetailPresenter<V extends DetailMvpView> extends BasePresenter<V>
        implements DetailMvpPresenter<V> {

    @Inject
    public DetailPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable,
                          ApiService apiService) {
        super(dataManager, schedulerProvider, compositeDisposable, apiService);
    }


    @Override
    public void onViewPrepared() {
        getMvpView().loadImage();
    }
}

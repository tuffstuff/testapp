package com.example.nursultan.testapp.ui.photo;

import com.example.nursultan.testapp.ui.base.MvpPresenter;

public interface PhotoMvpPresenter<V extends PhotoMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();

    void insertSearchQuery(String query);

    void getQueries();

    void requestSearchUpdate(String query);

    void goDetail(String path);
}

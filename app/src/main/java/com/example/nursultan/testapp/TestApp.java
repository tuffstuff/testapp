package com.example.nursultan.testapp;

import android.app.Activity;
import android.app.Application;

import com.example.nursultan.testapp.data.DataManager;
import com.example.nursultan.testapp.di.component.DaggerTestAppComponent;
import com.example.nursultan.testapp.di.component.TestAppComponent;
import com.example.nursultan.testapp.di.module.ContextModule;

import javax.inject.Inject;

import timber.log.Timber;

public class TestApp extends Application {

    @Inject
    DataManager mDataManager;

    private TestAppComponent component;

    public static TestApp get(Activity activity) {
        return (TestApp) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());

        component = DaggerTestAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

        component.inject(this);
    }


    public TestAppComponent getComponent() {
        return this.component;
    }
}

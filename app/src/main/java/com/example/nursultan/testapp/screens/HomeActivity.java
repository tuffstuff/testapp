package com.example.nursultan.testapp.screens;

import android.os.Bundle;

import com.example.nursultan.testapp.ui.photo.PhotoFragment;
import com.example.nursultan.testapp.R;
import com.example.nursultan.testapp.ui.base.BaseActivity;
import com.example.nursultan.testapp.ui.base.MvpView;

import butterknife.ButterKnife;


public class HomeActivity extends BaseActivity implements MvpView{

    private static final String TAG = "HomeActivity";
    private static final int COL_COUNT = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.frame_container, PhotoFragment.newInstance(COL_COUNT))
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    protected void setUp() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

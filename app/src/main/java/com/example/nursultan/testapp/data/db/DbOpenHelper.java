package com.example.nursultan.testapp.data.db;

import android.content.Context;

import com.example.nursultan.testapp.data.db.model.DaoMaster;
import com.example.nursultan.testapp.di.ApplicationContext;
import com.example.nursultan.testapp.di.DatabaseInfo;
import com.example.nursultan.testapp.di.TestApplicationScope;

import org.greenrobot.greendao.database.Database;

import javax.inject.Inject;
import javax.inject.Singleton;

@TestApplicationScope
public class DbOpenHelper extends DaoMaster.OpenHelper {

    @Inject
    public DbOpenHelper(@ApplicationContext Context context, @DatabaseInfo String name) {
        super(context, name);
    }

    // handle db migrations ? optional.
    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        switch (oldVersion) {
            case 1:
            case 2:
        }
    }
}

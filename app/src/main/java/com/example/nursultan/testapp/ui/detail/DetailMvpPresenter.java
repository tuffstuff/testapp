package com.example.nursultan.testapp.ui.detail;

import com.example.nursultan.testapp.ui.base.MvpPresenter;

public interface DetailMvpPresenter<V extends DetailMvpView>
        extends MvpPresenter<V> {
    void onViewPrepared();
}

package com.example.nursultan.testapp.ui.detail;

import com.example.nursultan.testapp.ui.base.MvpView;

public interface DetailMvpView extends MvpView {
    void loadImage();
}

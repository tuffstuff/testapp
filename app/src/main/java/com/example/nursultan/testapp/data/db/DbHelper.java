package com.example.nursultan.testapp.data.db;

import com.example.nursultan.testapp.data.db.model.Query;
import com.example.nursultan.testapp.dummy.DummyContent;

import java.util.List;

import io.reactivex.Observable;

public interface DbHelper {
    Observable<Long> insertQuery(final Query query);

    Observable<List<Query>> getAllQueries();

    // for sake of testing
    Observable<List<DummyContent.DummyItem>> getPhotos();
}

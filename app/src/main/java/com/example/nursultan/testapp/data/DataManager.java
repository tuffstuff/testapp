package com.example.nursultan.testapp.data;

import com.example.nursultan.testapp.data.db.DbHelper;

public interface DataManager extends DbHelper {
    void updateApiHeader();

}

package com.example.nursultan.testapp.ui.photo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nursultan.testapp.R;
import com.example.nursultan.testapp.dummy.DummyContent;
import com.example.nursultan.testapp.ui.base.BaseViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PhotoAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private Callback mCallback;
    private List<DummyContent.DummyItem> photoList;
    private final Picasso picasso;

    @Inject
    public PhotoAdapter(Picasso picasso) {
        this.picasso = picasso;
        photoList = new ArrayList<>();
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, parent, false));
    }

    @Override
    public int getItemCount() {
        if (photoList != null && photoList.size() > 0) {
            return photoList.size();
        } else {
            return 0;
        }
    }

    private DummyContent.DummyItem getItem(int pos){
        return photoList.get(pos);
    }

    public void swapItems(List<DummyContent.DummyItem> photos) {
        photoList.clear();
        photoList.addAll(photos);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onPhotoClick(DummyContent.DummyItem item);
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.photo_tv)
        TextView _photo_tv;
        @BindView(R.id.cell_iv)
        ImageView cell_iv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            cell_iv.setImageDrawable(null);
            _photo_tv.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final DummyContent.DummyItem item = photoList.get(position);
            picasso.load(item.path).into(cell_iv);

            if (item.title != null) {
                _photo_tv.setText(item.title);
            }


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (mCallback != null) {
                        DummyContent.DummyItem item = getItem(pos);
                        mCallback.onPhotoClick(item);
                    }
                }
            });
        }
    }
}

